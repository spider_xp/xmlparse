// rep1 project main.go
package main

import (
	"log"
	"strconv"
	"io/ioutil"
	"io"
	"unicode/utf8"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

type Attribute struct{
	name string
	typeof byte
	valueInt int
	valuefloat float64
	valueString string
	
}

type Attributes struct{
	values []Attribute
}

func (a Attributes) toString() string {
	ret := ""
	for i:= range a.values {
		switch {
			case a.values[i].typeof == 's':	ret = ret+" "+a.values[i].name+"=\""+a.values[i].valueString+"\"" ; break
			case a.values[i].typeof == 'i':	ret = ret+" "+a.values[i].name+"="+string(a.values[i].valueInt) ; break
			case a.values[i].typeof == 'f':	ret = ret+" "+a.values[i].name+"="+strconv.FormatFloat(a.values[i].valuefloat,'f',-1,32) ; break
		}
	}
	return ret
}

type Node struct{
	Name string
	Attrs Attributes
	Taxt string
	Childs []Node
	Path string
}

func parseName(data *[]byte, pos int) (string, int, error){
	ret:= ""
	curPos:= pos
	char,size:= utf8.DecodeRune((*data)[pos:])
	for char!= rune(' '){
		curPos+= size
		ret= ret+string(char)
		if curPos==len(*data) {return string(ret), curPos+1, io.EOF}
		char,size = utf8.DecodeRune((*data)[curPos:])
	}
	return string(ret), curPos+1, nil
}

func parseAttributes(data *[]byte, pos int) ([]Attributes, int, error){
	ret:= []Attributes{}
	curPos:= pos
}

func parseNode(data *[]byte, pos int, parent *Node) (int, *Node, error) {
	rNode := Node{}
	curPos := pos
	var err error
	rNode.Name,curPos,err = parseName(data, curPos); check(err)
	log.Println(rNode, curPos)
	return curPos, &rNode, nil
}

func main() {
//	var s string
	c := Attributes{}
	a := Attribute{}
	a.name="name1"
	a.typeof='s'
	a.valueString="attr1"
	b := Attribute{}
	b.name="name2"
	b.typeof='f'
	b.valuefloat=35.01
	d := Attribute{}
	d.name="name3"
	d.typeof='i'
	d.valueInt=53
	e := Attribute{}
	e.name="name4"
	e.typeof='d'
	e.valuefloat=35.01
	c.values = []Attribute{a,b,d,e}
	doc, err := ioutil.ReadFile("document.xml_");check(err)
	xml := Node{}
	_,_, err = parseNode(&doc ,0, &xml)
	check(err)
	log.Println(c.toString())
}
